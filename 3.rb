# 4/22
# 今週の最終目標
# soundhouseからメーカー名と型番と価格をとってくる
# 使う技術::クラスを作って利用する、ファイルの入出力(済)、正規表現

# メーカー名、型番、価格をインスタンス変数として持っているクラスをつくる
# クラス名 Effector
# maker_name
# model_name
# price
# イニシャライザーでインスタンス変数を初期化
# アトリビュートリーダー(attr_reader)だけ持っておく

class Effector
  attr_reader :maker_name, :model_name, :price
  #attr_readerはインスタンス変数を返すメソッドをつくる

  def initialize(maker_name,model_name,price)
    @maker_name = maker_name
    @model_name = model_name
    @price = price
  end
end

effectors = []
effectors << Effector.new('メーカー','モデル名',"1,000円") #Effectorクラスのインスタンスを生成
effectors << Effector.new('メーカー2','モデル名2',"1,000円") #Effectorクラスのインスタンスを生成
effectors << Effector.new('メーカー3','モデル名3',"1,000円") #Effectorクラスのインスタンスを生成
effectors.each {|effector| p effector }

#@name = ""は後から設定するため
