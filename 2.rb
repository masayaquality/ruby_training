# # Rubyでディレクトリ(mizo-)のファイルすべてを開き、特定の文字列を置換して(cookpad→retty)、保存する。
# #
# # Dir::foreach('mizoguchi_lesson2'){|abc|
# #   puts"#{abc}: #{File:abc(f)}"
# # }
#
#
PATH = "/users/masaya/Desktop/mizoguchi_lesson2/"


dir =  Dir::entries(PATH)

dir.each do|filename|
  filepath =  "#{PATH}#{filename}"
  p filepath
  next if filename == "." || filename == ".."
  f = open(filepath)
  content =  f.read # f.readの返り値はstring
  f.close
  # Stringクラスでファイル開くのは無理→ファイルクラス
  content = content.gsub("cookpad","retty")
  # 変数は箱　contentという変数に、gsubの返り値を入れる
  puts content
  File.write(filepath,content)

  #gsub!はレシーバが変更される

end


#
# # ファイル名１つ１つをフルパスで表示
#
# p(dir)
#
# dir.gsub("cookpad" =>"retty")
#
# puts dir
#
#
# # Arrayのひとつひとつに処理をする
#
#
#
#
# # p は文字列を標準出力する　デバッグ用
# # null,nilは値がない(言語によって違う)
# # classがメソッドを持っている
# # dir はArrayクラスのインスタンス
# # クラスにメソッドがない
# # Arrayにはgsubは定義されていない
#
#
# class Review #クラス
#   def abc #メソッド
#     puts "aaa"
#   end
# end
#
# class Reviews #クラス
#   def length #メソッド
#     # もってるReviewクラスのインスタンスの数を返す
#   end
# end
