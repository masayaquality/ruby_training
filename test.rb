# # # # 引数に数字をもらって、その数字に2をかけて、返すメソッドを書く
# # # # メソッド名:twice
# # #
# # # 引数に数字をもらう
# # 引数でもらった数字に２をかけて返す
#
# def twice(numbers)
#   numbers * 2
# end
# puts twice(10)
#
#
# # Rubyでディレクトリ(mizo-)のファイルすべてを開き、
#特定の文字列を置換して(cookpad→retty)、保存する。

dir = Dir::entries("/users/masaya/Desktop/mizoguchi_lesson2")
# ディレクトリ内のファイルをすべて呼び出す
dir.each do|filename|
  filepath = "/users/masaya/Desktop/mizoguchi_lesson2/#{filename}"
  # 文字列を変数に代入する
  next if filename == "." || filename == ".."
  f = open(filepath)
  # openメソッド、引数で渡された文字列のファイルを開いて、fileクラスのインスタンスで返す
  # .と..を開くのをやめているわけではない
  content = f.read #.readの返り値はstring
  # f.close #fileを閉じている()
  # stringクラスでgsubメソッドは使える
  # なので、変数にいれる
  replaced_content = content.gsub("retty","cookpad")
  puts f.read.gsub("retty","cookpad")
end

# dirにgdubをつけようとしていた→dirはarrayクラスなのでダメ
# その行で何が行われているのか
# 変数名のつけかたでエンジニア力がつける！「リーダブルコード」
